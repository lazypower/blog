---
title: "Self Hosted Apps on Kubernetes"
date: 2017-10-30T18:52:39-05:00
subtitle: 
tags: [kubernetes, self-hosted]
type: post
---

2017 has been a strange year for technology. Commercial interests seem intent
on pushing further into the market with new flashy features designed to entice
you to spend more time on their properties, so they can harvest even more detail
about what you may be interested in right now so they can bombard you with
advertisements.

<!--
This is a clear model of profitability on the internet. I'm no stranger to this
methodology having cut my teeth in the IT world at a startup marketing agency.
At this aformentioned markeing agency we did everything from retargeting, social
media mining, to cleverly disguised landing pages to convince you to give us your
credentials so we could run your information through every subsystem we had
access to in the name of:

- Qualifying your info as a lead to partners
- Matching you with stated interests
- Hounding you to get your $ for our partners so they would pay us more per lead.

In retrospect, I feel very awkward about my part in developing the technology that
empowered that. I can't say that I regret the experience, but I do have some 
personal feelings of "sell out" with regard to the matter. Alas, this brings me
to my topic today.
-->

It's a well understood paradigm that if you dont pay for the service (and even
in some cases, if you pay you're still victim of this) you *are* the product.
How do I take reduce my footprint you say? Well I'm glad you asked! This is where
self-hosting can play a big role in your net-surface-reduction in where third
parties can track you.


### I'm a self hoster, and this will be our journey

As nobody really seems to *care* about the health of the distributed web, its
up to us to stay stalwart in the face of rampant corporate takeover of our data.
I've been spending some time putting together resources to self-host my own
suite of applications and have amassed a nice little suite of docker containers
and helm charts for hosting my self-hosted adventure.

In the coming days I'll be writing a few tutorials and launching resources that
you may use to start your own self-hosted adventure.

I'll take the following format of the posts so they are easily digestible in
chunks and can be done in series, or in a weekend if you're a late-starter
and want to piece together you own setup.

- Introduction: I'll give a brief overview of the application itself, and what
services it can replace/augment in your workflow.
- Resources used to create the workload.
  - This is anything from upstream documentation, to stray blog posts that held
    key information to operating the workload.
- The workload itself, in its entirety w/ private values stubbed out where appropriate.
- Links to continue learning or discuss

If you find the notion of doing server maintenance a nail-biting scenario, I
wont lie to you, that it's a bit of an investment. However; it doesn't have to
be super scary and the management of the system doesn't have to be so time
consuming that its a full time second job.

I will try to maintain that the object here is that any hobbyist can, and should
learn how to jockey their own self-hosted application, and I will help you
achieve that goal along the way.


### Pre-reqs

I would suggest that we start with a baseline. I'll presume that you're not a
kubernetes expert, and have only marginal experience in working with Docker.
You understand the fundamentals that Containers are pre-packaged runtimes (or
a virtualized operating system w/ bundled software) that you can execute in
many scenarios to deliver, run, and operate software.

If you're looking for a straight forward way to get started, I can recommend
a few routes, and this is not reflective of the entire landscape as there are
80+ vendors of Kubernetes today:

- [Google Kontainer Engine (GKE)](https://cloud.google.com/container-engine/) - This is googles hosted solution in Google Cloud.
Its probably the most straight forward, but also the most expensive option of anything
listed.

- [KubeADM](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/) KubeADM is the up-and-coming official way to get started with kubernetes outside of GKE

- [Typhoon](https://typhoon.psdn.io/digital-ocean/) is a method to deploy Kubernetes on Digital Ocean using Terraform.

- [Canonical Distribution of Kubernetes (CDK)](https://jujucharms.com/kubernetes) - Juju (Ubuntu) powered Kubernetes anywhere Juju is supported.


This in itself can be a weekend long project if you are new to Kubernetes, but
dont let it scare you. All of the assets we will be using here are also going
to run just as well on a self-hosted [minikube](https://github.com/kubernetes/minikube)
installation so you can validate things on your laptop before you spend any
money running this in the cloud, or take over your pc in your closet (this
is where i started, self hosted on premis on an old i7 desktop).

With this, I'll leave off and pick up again later this week with another
post introducing our first workload - [Nextcloud](https://nextcloud.com/) - A safe
home for all your data, as a self hosted alternative to file services like Dropbox,
google cloud storage, and several others.

Until then, Happy hacking!


